import initServicesSlider from "./modules/commonSlider";
import initModal from "./modules/modalHome";
import Forms from "./modules/formValidation";
import scrollTo from "./modules/scrollTo";

window.addEventListener("DOMContentLoaded", () => {
  initServicesSlider()
  initModal()
  scrollTo()
  new Forms()
})