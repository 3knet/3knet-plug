export default function initModal() {
  const modal = document.querySelector(".modal")
  const modalContainer = modal.querySelector(".modal__container")
  const btnBusiness = document.querySelectorAll(".business__card-button")
  const srvButtons = document.querySelectorAll(".services__card-button")
  const serviceInput = document.querySelectorAll(".form__input-service")
  const header = document.querySelector(".header")
  const inputs = document.querySelectorAll(".form__input")
  const formContainer = document.querySelectorAll(".form__container")
  
  function openModal(btn) {
    header.classList.add("header_disabled")
    modal.classList.add("modal_active")
    serviceInput.forEach((input) => {
      input.value = btn.getAttribute("data-service")
    })
  }
  
  function handleCloseByKey(evt) {
    if(evt.key === "Escape") {
      header.classList.remove("header_disabled")
      modal.classList.remove("modal_active")
      serviceInput.forEach((input) => {
        input.value = ""
      })
      inputs.forEach(input => {
        input.value = ""
      })
      const formSuccess = document.querySelectorAll(".form__success")
      formSuccess.forEach((form) => {
        form.classList.remove("form__success_active")
      })
      formContainer.forEach((formCont) => {
        formCont.classList.remove("form__container_disable")
      })
    }
  }
  
  function handleClose(evt) {
    if(evt.target === modalContainer) {
      header.classList.remove("header_disabled")
      modal.classList.remove("modal_active")
      serviceInput.forEach((input) => {
        input.value = ""
      })
      const formSuccess = document.querySelectorAll(".form__success")
      inputs.forEach(input => {
        input.value = ""
      })
      formSuccess.forEach((form) => {
        form.classList.remove("form__success_active")
      })
      formContainer.forEach((formCont) => {
        formCont.classList.remove("form__container_disable")
      })
    }
  }
  
  btnBusiness.forEach((btn) => {
    btn.addEventListener("click", () => openModal(btn))
    return () => btn.removeEventListener("click", openModal)
  })
  
  srvButtons.forEach((btn) => {
    btn.addEventListener("click", () => openModal(btn))
    return () => btn.removeEventListener("click", openModal)
  })
  
  window.addEventListener("click", handleClose)
  window.addEventListener("keydown", handleCloseByKey)
  
  return () => {
    window.removeEventListener("click", handleClose)
    window.removeEventListener("keydown", handleCloseByKey)
  }
}