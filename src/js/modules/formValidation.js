import 'parsleyjs';
import IMask from "imask";

class Forms {
  constructor() {
    this.forms = [...document.querySelectorAll(".form")]
    this.inputs = [...document.querySelectorAll(".form__input")]
    this.phoneInputs = document.querySelectorAll(".phone-js")
    this.formSuccess = document.querySelectorAll(".form__success")
    this.formContainer = document.querySelectorAll(".form__container")
    
    this.btnClose = document.querySelectorAll(".form__success-btn")
    this.modal = document.querySelector(".modal")
    
    this.header = document.querySelector(".header")

    this.initMasks()
    this.submitData()
    this.handleClose()
  }
  
  handleCloseByKey(evt) {
    if(evt.key === "Escape") {
      this.header.classList.remove("header_disabled")
      this.modal.classList.remove("modal_active")
    }
  }
  
  initMasks() {
    this.phoneInputs.forEach((input) => {
      IMask(input, {mask: '+{7}(000)000-00-00'})
    })
  }
  
  submitData() {
    this.forms.forEach((form) => {
      const formSuccess = form.querySelector(".form__success")
      form.addEventListener("submit", (evt) => {
        evt.preventDefault();
        
        $(form).parsley().validate();
        if ($(form).parsley().isValid()) {
          const url = form.action;
          const formData = new FormData(form)
          fetch(url, {
            method: "POST",
            body: formData
          })
            .then((res) => {
              if(res.ok) {
                formSuccess.classList.add("form__success_active")
                this.formContainer.forEach((formCont) => {
                  formCont.classList.add("form__container_disable")
                })
              } else {
                formSuccess.classList.add("form__success_active")
                this.formContainer.forEach((formCont) => {
                  formCont.classList.add("form__container_disable")
                })
              }
            })
            .catch((err) => console.log(err))
        }
      })
    })
  }
  
  handleClose() {
    const btnClose = document.querySelectorAll(".form__success-btn")
    const formSuccess = document.querySelectorAll(".form__success")
    if(!btnClose || !formSuccess) return
    
    btnClose.forEach((btn) => {
      btn.addEventListener("click", () => {
        formSuccess.forEach((form) => {
          form.classList.remove("form__success_active")
        })
        this.inputs.forEach(input => {
          input.value = ""
        })
        this.modal.classList.remove("modal_active")
        this.header.classList.remove("header_disabled")
        this.formContainer.forEach((formCont) => {
          formCont.classList.remove("form__container_disable")
        })
      })
    })
  }
  
}

export default Forms