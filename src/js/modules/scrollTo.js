export default function scrollTo() {
  const introBtn = document.querySelector(".intro__button")
  const contactScroll = document.getElementById("contact-js")
  
  introBtn.addEventListener("click", () => {
    contactScroll.scrollIntoView({behavior: "smooth", block: "start", inline: "start"})
  })
}