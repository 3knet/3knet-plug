import Swiper from "swiper";
import {Navigation} from "swiper";
import {tablet} from "../constants/constants";

export default function initServicesSlider() {
  const modules = [Navigation]
  const mql = window.matchMedia(`(max-width: ${tablet}px)`)
  
  const servicesSwiper = document.querySelector(".services__swiper")
  const businessSwiper = document.querySelector(".business__swiper")
  const slidersArr = [servicesSwiper, businessSwiper]
  
  if(!servicesSwiper || !businessSwiper) return
  
  slidersArr.forEach((slider) => {
    if(mql.matches) {
      const swiper = new Swiper(slider, {
        slidesPerView: 2.2,
        modules: modules,
        spaceBetween: 10
      })
      swiper.init()
    }
  })
}